export class PostfixCalculator {

    public eval(expression: string): number {
        let stack = [];
        let token = "";
        for (let c of  expression) {
            if (!isNaN(Number(c) )) {
                token += c;
                continue;
            }

            if (token.length > 0) {
                stack.push(parseFloat(token));
                token = ""
            }

            if (c == '+') {
                stack.push(stack.pop() + stack.pop())
            } else if (c == '-') {
                stack.push(-stack.pop() + stack.pop())
            } else if (c == '*') {
                stack.push(stack.pop() * stack.pop())
            } else if (c == '/') {
                stack.push(1/stack.pop() * stack.pop())
            }
        }

        if (token.length > 0) {
            let operand = parseFloat(token);
            stack.push(operand);
        }
        return stack[stack.length - 1];
    }
}